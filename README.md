# Overview and usage

This is a set of shell scripts that run and benchmark different nameserver implementations.

## Executable files

* `dnsbench.sh` - the main executable, sets up all the boxes and runs measurements.
* `measure.sh`  - performs measurement on the current box based on the .tgz dataset.
* `netsetup.sh` - (optional) preps network settings, tweaking things like flow control.

## What it roughly does

This framework expects three boxes. One control box for the benchmark orchestration.
And two separate but directly interconnected boxes - one acting as a server
and one replaying DNS traffic.

Why is this better than dnsperf or other tools? Well it doesn't selfpace and the
nameserver implementation is forced to process what is being thrown at it,
which simulates what happens in real world during an attack fairly well.

This benchmarking toolset does the preparation, setup, and the dirty work for you,
but you are still in control of what and how it should be tested. There are two
things you can change:
* Dataset - essentially a zone file(s), config files for tested servers, and queries file
* Test config - configuration of the test environment, replay rates and such.

## What you need

* server - ``bc`` and all the neccessary things to build the nameservers from sources
  or their binaries installed
* player - ``kxdpgun``

## How to run tests

* First make sure that pasword-less key-based ssh works between the boxes.
* See the sample configuration `conf/sample.conf` and specify proper hostnames,
  addresses, interfaces etc.
* If you didn't build any dataset before, compile them (e. g. `cd datasets/tld; ./generate.sh`).
* To try it out, try something like `./dnsbench.sh conf/my.conf datasets/tld.tgz`
