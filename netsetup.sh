#!/usr/bin/env bash

XDP="no"
KSOFTIRQD="no"
FAST="no"
VERBOSE="no"
MLNX_TUNE_MODE="HIGH_THROUGHPUT"
ETH_QUEUES=""
DEV=""
DRIVER=""
IRQS_DEV=""

DEVICES="$(ip link show | grep -v LOOPBACK | awk -F': ' '/^[0-9]+: /{print "    " $2}')"

available_devices()
{
	echo "Available devices:"
	echo "$DEVICES"
	echo ""
}

usage()
{
	echo "Description:"
	echo "    This script optimizes network card configuration"
	echo ""
	echo "Usage:"
	echo "    $0 [OPTION]... [DEV]"
	echo ""
	echo "Options:"
	echo "    -h, --help"
	echo "        Show this message."
	echo ""
	echo "    -q, --queues[=num_of_queues]"
	echo "        Specify the number of combined queues (defaults to maximum)."
	echo ""
	echo "    -x, --xdp"
	echo "        Hard IRQs are deferred and handled in a separate softirq "
	echo "        context, improving parallelism and reducing latency."
	echo "        Packets are also coalesced into larger ones and sent into"
	echo "        application every 200 ms."
	echo ""
	echo "    --rx_bufs[=num_of_buffers]"
	echo "        Sets the size of the receive (RX) ring buffer to \'num_of_buffers\' packets."
	echo ""
	echo "    --tx_bufs[=num_of_buffers]"
	echo "        Sets the size of the transmit (TX) ring buffer to \'num_of_buffers\' packets."
	echo ""
	echo "    -k, --ksoftirqd"
	echo "        Lowers the CPU scheduling priority of ksoftirqd processes "
	echo "        (which handle network soft IRQs) to a value of 19, reducing "
	echo "        their impact on system performance."
	echo ""
	echo "Special Modes:"
	echo "    -m, --msix"
	echo "        Uses mlxconfig to configure the number of MSI-X interrupt vectors (NUM_PF_MSIX)"
	echo "        for the specified PCI device to the maximum allowed value and exits."
	echo ""
	echo "    -f, --fast"
	echo "        Lightweight mode that configures an active network interface without unloading it."
	echo "        Only performs a part of the standard configuration."
	echo ""
	echo "    -v, --verbose"
	echo "        Writes debug information to stdout."
	echo ""

	available_devices
}

try_run()
{
	if [ "${VERBOSE}" == "no" ]; then
		"$@" &> /dev/null
	else
		if [ "${1}" == "bash" ] && [ "${2}" == "-c" ]; then
			echo "\$ ${@:3}"
		else
			echo "\$ ${@}"
		fi
		out="$("$@" 2>&1)"
		if [ -n "${out}" ];then
			echo "${out}" | sed 's/^/|-  /'
		fi
	fi

        if [ $? != 0 ]; then
                echo "ERROR: '$@'"
        fi
}

adjust_queues()
{
	MAX_QUEUES="$(ethtool -l $DEV | grep Combined | awk -F'Combined:	' 'NR==1{print $2}')"
	if [[ $ETH_QUEUES =~ ^[0-9]+$ ]]; then
		if [ $ETH_QUEUES -gt $MAX_QUEUES ]; then
			echo "WARNING: The number of queues has been trimmed to $MAX_QUEUES"
			ETH_QUEUES=$MAX_QUEUES
		fi
	else
		ETH_QUEUES=$MAX_QUEUES
	fi
}

unload_xdp()
{
	try_run ip link set dev "$DEV" xdp off
}

mellanox_prepare()
{
	if [ $ETH_QUEUES -lt $MAX_QUEUES ]; then
		DRIVER_DEV="mlx5_"
	else
		DRIVER_DEV="mlx5_comp"
	fi
	PCI_DEV="$(ethtool -i $DEV | grep bus-info | awk -F ': ' '{print $2}')"
	IRQS_DEV="${DRIVER_DEV}.*${PCI_DEV}"
}

intel_prepare()
{
	DIR="TxRx"
	IRQS_DEV="$DEV.*$DIR"
}

mellanox_ethtool_setup()
{
	adjust_queues

	if [[ $ETH_QUEUES =~ ^[0-9]+$ ]]; then
		if [ $ETH_QUEUES -gt $MAX_QUEUES ]; then
			ETH_QUEUES=$MAX_QUEUES
		fi
	else
		ETH_QUEUES=$MAX_QUEUES
	fi

	if ! [[ $RX_BUFS =~ ^[0-9]+$ ]]; then
		RX_BUFS=512
	fi

	if ! [[ $TX_BUFS =~ ^[0-9]+$ ]]; then
		TX_BUFS=512
	fi

	if [ $FAST != "yes" ]; then
		try_run ethtool -L $DEV combined ${ETH_QUEUES}
	fi
	try_run ethtool -C $DEV adaptive-rx on adaptive-tx off
	try_run ethtool -A $DEV autoneg off rx off tx off
	try_run ethtool -K $DEV tso off gro off ufo off
	if [ $FAST != "yes" ]; then
		try_run ethtool -G $DEV rx $RX_BUFS tx $TX_BUFS
	fi
}

i40e_ethtool_setup()
{
	adjust_queues

	if ! [[ $RX_BUFS =~ ^[0-9]+$ ]]; then
		RX_BUFS=256
	fi

	if ! [[ $TX_BUFS =~ ^[0-9]+$ ]]; then
		TX_BUFS=512
	fi

	TX_USECS=6
	RX_USECS_HIGH=236
	TX_FRAMES_IRQ=64

	if [ $FAST != "yes" ]; then
		try_run ethtool -L $DEV combined ${ETH_QUEUES}
	fi
	try_run ethtool -A $DEV rx off tx off
	try_run ethtool -K $DEV tso off gro off ufo off
	if [ $FAST != "yes" ]; then
		try_run ethtool -G $DEV rx ${RX_BUFS} tx ${TX_BUFS}
	fi
	try_run ethtool -C $DEV adaptive-rx on adaptive-tx off
	try_run ethtool -C $DEV tx-frames-irq ${TX_FRAMES_IRQ} rx-usecs-high ${RX_USECS_HIGH} tx-usecs ${TX_USECS}
	try_run ethtool -N $DEV rx-flow-hash udp4 sdfn
	try_run ethtool -N $DEV rx-flow-hash udp6 sdfn
}

ice_ethtool_setup()
{
	adjust_queues

	if ! [[ $RX_BUFS =~ ^[0-9]+$ ]]; then
		RX_BUFS=1024
	fi

	if ! [[ $TX_BUFS =~ ^[0-9]+$ ]]; then
		TX_BUFS=512
	fi

	if [ $FAST != "yes" ]; then
		try_run ethtool -L $DEV combined ${ETH_QUEUES}
	fi
	try_run ethtool -A $DEV rx off tx off
	try_run ethtool -K $DEV tso off gro off ufo off
	if [ $FAST != "yes" ]; then
		try_run ethtool -G $DEV rx ${RX_BUFS} tx ${TX_BUFS}
	fi
	try_run ethtool -C $DEV adaptive-rx on adaptive-tx off tx-usecs 8
	try_run ethtool -N $DEV rx-flow-hash udp4 sdfn
	try_run ethtool -N $DEV rx-flow-hash udp6 sdfn
}

set_irq_affinity()
{
	IRQS=$(egrep $IRQS_DEV /proc/interrupts | cut -d: -f1 | sed "s/ //g")
	if [ -z "$IRQS" ] ; then
		echo no $DIR vectors found on $DEV
		return
	fi

	cnt=1
	val=1
	block=0
	for IRQ in ${IRQS}; do
		MASK=$(printf "%08X" ${val})
		for (( j=1; j<=block; j++ )); do
			MASK="${MASK},00000000"
		done
		if [ $cnt -eq 32 ]; then
			val=1
			block=$((block+1))
			cnt=1
		else
			val=$((val*2))
			cnt=$((cnt+1))
		fi
		try_run bash -c "echo $MASK | sudo tee /proc/irq/$IRQ/smp_affinity"
	done
}

set_ksoftirqd_priority()
{
	priority=0
	if [ "$KSOFTIRQD" == "yes" ]; then
		priority=19
	fi

	try_run renice -n $priority -p $(pgrep '^ksoftirqd/[0-9]*$' | head -n ${ETH_QUEUES})
}

set_irq_defer()
{
	DEFER_PATH="/sys/class/net/$DEV/napi_defer_hard_irqs"
	FLUSH_PATH="/sys/class/net/$DEV/gro_flush_timeout"

	defer=0
	flush=0
	if [ $XDP == "yes" ]; then
		defer=2
		flush=200000
	fi

	try_run bash -c "echo $defer | sudo tee $DEFER_PATH"
	try_run bash -c "echo $flush | sudo tee $FLUSH_PATH"                         
}

tune_linux() {
	if [ -n "$(pidof irqbalance)" ]; then
		echo "Service irqbalance has to be disabled!"
		exit 1
	fi

	if [ $FAST != "yes" ]; then
		unload_xdp
	fi

	case "$DRIVER" in
		mlx5_core)
			mellanox_ethtool_setup
			mellanox_prepare
			if [ $FAST != "yes" ]; then
				try_run mlnx_tune -p ${MLNX_TUNE_MODE}
			fi
			;;
		i40e)
			i40e_ethtool_setup
			intel_prepare
			;;
		ice)
			ice_ethtool_setup
			intel_prepare
			;;
		*)
			echo "Unsupported driver ($DRIVER)!"
			echo ""
			available_devices
			exit 1
			;;
	esac

	set_irq_affinity

	set_ksoftirqd_priority

	set_irq_defer
}

mellanox_mlxconfig()
{
	PCI_DEV="$(ethtool -i $DEV | grep bus-info | cut -d':' -f3-)"
	MAX_MSIX="$(sudo lspci -vv -s $PCI_DEV | grep -oP 'MSI-X:.*Count=\K\d+')"

	sudo mlxconfig -d $PCI_DEV s NUM_PF_MSIX=$((MAX_MSIX - 1))
}

if [ `uname` != 'Linux' ]; then
	echo "Linux OS required!"
	exit 1
fi

args=$(getopt -o q:xhkmfv --long queues:,xdp,rx_bufs:,tx_bufs:,help,ksoftirqd,msix,fast,verbose -- "$@")
if [[ $? -gt 0 ]]; then
	usage
	exit 1
fi

eval set -- ${args}

while :
do
	case $1 in
		-q | --queues)
			ETH_QUEUES=$2
			shift 2
		;;
		-x | --xdp)
			XDP="yes"
			shift
		;;
		-k | --ksoftirqd)
			KSOFTIRQD="yes"
			shift
		;;
		-m | --msix)
			while [ "$1" != "--" ]; do
				shift
			done
			shift	
			DEV="$1"
			mellanox_mlxconfig
			exit 0
		;;
		-f | --fast)
			FAST="yes"
			shift
		;;
		-v | --verbose)
			VERBOSE="yes"
			shift
		;;
		--rx_bufs)
			RX_BUFS=$2
			shift 2
		;;
		--tx_bufs)
			TX_BUFS=$2
			shift 2
		;;
		\? | -h | --help)
			usage
			exit 0
		;;
		--)
			shift
			break
		;;
		*)
			exit 1
		;;
	esac
done

if [[ -z "$1" || "$1" == -* ]]; then
	usage
	exit 0
else
	DEV="$1"
	shift

	if ! echo "$DEVICES" | grep -w -q "$DEV"; then
		echo "Not a valid device!"
		echo ""
		available_devices
		exit 0
	fi
fi

if [ ! "$(id -u)" -eq 0 ]; then
	echo "This script needs to be run as a root!"
	exit 1
fi

DRIVER="$(ethtool -i $DEV 2>/dev/null | grep driver | awk -F ': ' '{print $2}')"

tune_linux

echo "$(uname -sr) / $DEV / $DRIVER / $ETH_QUEUES queues"

