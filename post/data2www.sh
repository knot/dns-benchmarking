#!/usr/bin/env bash

DATADIR=$(pwd)
RESDIR="${DATADIR}/results"

data="${1}"

# Requirements
if ! which markdown &>/dev/null; then
	echo "error: 'markdown' not found"
	exit 1
fi

# Unpack data and start conversion
tmpdir=$(mktemp -d)
cd ${tmpdir}
tar xzf ${data}

# Fetch metadata
slug=$(basename ${data}); slug="${slug%%.tgz}"
meta=( $(echo ${slug}|tr "-" " ") )
info=( $(cat INFO) )
kversion=${info[1]}
kversion=${kversion%%-*}

wwwdir="${RESDIR}/${meta[0]}-${meta[1]}-${meta[2]}"

# Convert column data to list
function extract_col() {
	categories=$(tail -n +2 ${1}|cut -f${2}|tr "\n" ",")
	echo "${categories%%,}"
}

# Write out usage data
written=0
for usage_type in usage ddns; do
	if [ ! -f ${usage_type} ]; then
		continue
	fi

	outfile="${wwwdir}/${meta[0]}-${meta[1]}-${usage_type}.js"
	categories=$(extract_col ${usage_type} 1)

	# Plot name must correspond to heading
	name=""
	case ${usage_type} in
	usage) name="resource-usage" ;;
	ddns)  name="ddns-query-processing" ;;
	esac

	# Write out header
	cat > ${outfile} << EOF
var ${usage_type}_chart = {
	'type': '${name}',
	'slug': '${meta[0]}-${meta[1]}-${usage_type}',
	'date': '$(date +%Y-%m-%d)',
    	'note': '$(cat NOTES|markdown|sed -e "s/'/\\\'/g"|tr "\n" " ")',
	'properties': {
		'os': '${info[0]} ${kversion}',
		'deployment': '$(head -n 1 NOTES|tr -d "\n")',
	},
	'categories': [ ${categories} ],
EOF
	# Write subcategories for DDNS
	if [ ${usage_type} == "ddns" ]; then
		echo -n "'subcategories': [ " >> ${outfile}
		info=$(head -n1 ${usage_type}); info=( $(echo ${info:2}) )
		for rr_changed in ${info[@]}; do
			echo -n "\"${rr_changed##0} records changed\", " >> ${outfile}
		done
		echo "]," >> ${outfile}

		# Write out data series
		echo "'series': [" >> ${outfile}
		echo -e "\t{ name: 'large zone (DNSSEC)', data: [ $(extract_col ddns 2) ] }," >> ${outfile}
		echo -e "\t{ name: 'large zone (unsigned)', data: [ $(extract_col ddns 3) ] }," >> ${outfile}
		echo -e "\t{ name: 'small zone (DNSSEC)', data: [ $(extract_col ddns 4) ] }," >> ${outfile}
		echo -e "\t{ name: 'small zone (unsigned)', data: [ $(extract_col ddns 5) ] }" >> ${outfile}
	else

		# Write out data series
		echo "'series': [" >> ${outfile}
		echo -e "\t{ stack: 'time',   name: 'Zone compilation', data: [ $(extract_col usage 2) ] }," >> ${outfile}
		echo -e "\t{ stack: 'time',   name: 'Server startup', data: [ $(extract_col usage 3) ] }," >> ${outfile}
		echo -e "\t{ stack: 'memory', name: 'Memory usage (Rss)', data: [ $(extract_col usage 4) ] }," >> ${outfile}
		echo -e "\t{ stack: 'memory', name: 'Memory usage (Swap)', data: [ $(extract_col usage 5) ] }" >> ${outfile}
	fi

	# Write rest
	echo -e "]};\n\nKNOT.rawChartData.push(${usage_type}_chart);" >> ${outfile}

	# Message
	echo "converted '${usage_type}' -> $(basename ${outfile})"
	(( written += 1 ))
done

# Exit if no RR data
if ! ls *.data &>/dev/null; then
	cd - &>/dev/null
	rm -rf ${tmpdir}
	exit 0
fi

# Write header
echo "converting RR results for '${meta[@]}'"
deployment_prefix=""
if grep "Protocol:" NOTES | grep -q "TCP"; then
	deployment_prefix="<b>TCP</b> "
fi
outfile="${wwwdir}/${slug}.js"
cat > ${outfile} << EOF
var chart = {
    'type': 'response-rate',
    'slug': '${slug}',
    'date': '$(date +%Y-%m-%d)',
    'note': '$(cat NOTES|markdown|sed -e "s/'/\\\'/g"|tr "\n" " ")',
    'properties': {
        'os': '${info[0]} ${kversion}',
        'deployment': '${deployment_prefix}$(head -n 1 NOTES|tr -d "\n")',
        'adapter': '$(eval echo ${info[@]:2})'
    },
    'series': [
EOF

ip_size='46'
[[ $slug = *ipv6* ]] && ip_size='66'

# Write data series
for d in *.data; do
	[ ${written} -gt 0 ] && echo " , " >> ${outfile}
	(( written += 1 ))

	# Create line name
	info=$(head -n1 ${d}); info=${info:2}
	name=$(echo ${info}      | awk -F',' '{print $1}')
	resp_size=$(echo ${info} | awk -F',' '{print $2}')
	ethtool=$(echo ${info}   | awk -F',' '{print $3}')

	# Create list of [rate, percentage]
	data=""
	while read -r pair; do
		data="${data} [${pair}],"
        done < <(tail -n +2 ${d}|awk '{print $1,$2}'|tr " " ",")

	# Write out line data
	echo "{ name: '${name}', custom: { ip_size: ${ip_size}, avg_resp_size: ${resp_size}}, data: [ ${data[@]} ] }" >> ${outfile}
done

# Write rest
cat >> ${outfile} << EOF
]};

KNOT.rawChartData.push(chart);
EOF

# Stats
echo "converted '${written}' files to JSON in '${wwwdir}'"


# Remove working directory
cd - &>/dev/null
rm -rf ${tmpdir}
