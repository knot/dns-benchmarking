#!/usr/bin/env bash
set -e

zone="se"
ref="tld_dnssec"

ln -s ../${ref}/zones
../../tools/gen_zonelist.sh
../../tools/gen_configs.sh
../../tools/gen_nxdomain.sh ../${ref}/querydb ${zone} 100
../../tools/gen_notes.sh "TLD NSEC3" 1 NXDOMAIN 100 100
../../tools/pack_dataset.sh
