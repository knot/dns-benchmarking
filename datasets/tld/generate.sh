#!/usr/bin/env bash
set -e

zone="se"

mkdir -p zones
dig_opts="+onesoa +noclass +nocomments +nostats +nocmd"
dig AXFR ${dig_opts} @zonedata.iis.se ${zone} | egrep -v "\sRRSIG\s|\sNSEC|\sDNSKEY\s" > ./zones/${zone}.zone
../../tools/gen_zonelist.sh
../../tools/gen_configs.sh
../../tools/gen_noerror.sh ./zones/${zone}.zone
../../tools/gen_notes.sh "TLD" 1 NOERROR 100 0
../../tools/pack_dataset.sh
