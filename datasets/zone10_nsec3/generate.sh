#!/usr/bin/env bash
set -e

zone="zone10"

mkdir -p zones
BASEDIR=./zones ../../tools/sign_zone.sh ${zone} ../${zone}/zones/${zone}.zone ./zones/${zone}.zone nsec3
../../tools/gen_zonelist.sh
../../tools/gen_configs.sh
../../tools/gen_noerror.sh ./zones/${zone}.zone
sed -i -E 's/$/\ DO/' querydb
mv querydb querydb_do
../../tools/gen_nxdomain.sh querydb_do ${zone} 100
../../tools/gen_notes.sh "Zone 10 NSEC3" 1 NXDOMAIN 100 100
../../tools/pack_dataset.sh
