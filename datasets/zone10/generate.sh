#!/usr/bin/env bash
set -e

zone="zone10"

mkdir -p zones
sed -e "s%@DOTZONE@%.${zone}.%g" -e "s%@ZONE@%${zone}.%g" ../../tools/zone.tpl > ./zones/${zone}.zone
../../tools/gen_zonelist.sh
../../tools/gen_configs.sh
../../tools/gen_noerror.sh ./zones/${zone}.zone
../../tools/gen_notes.sh "Zone 10" 1 NOERROR 100 0
../../tools/pack_dataset.sh
