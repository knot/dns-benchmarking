#!/usr/bin/env bash
set -e

zone="se"
ref="tld"

mkdir -p zones
BASEDIR=./zones ../../tools/sign_zone.sh ${zone} ../${ref}/zones/${zone}.zone ./zones/${zone}.zone nsec3
../../tools/gen_zonelist.sh
../../tools/gen_configs.sh
cat ../${ref}/querydb | sed -E 's/$/\ DO/' > querydb
../../tools/gen_notes.sh "TLD DNSSEC" 1 NOERROR 100 100
../../tools/pack_dataset.sh
