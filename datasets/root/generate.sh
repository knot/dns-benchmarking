#!/usr/bin/env bash
set -e

zone="."

mkdir -p zones
dig_opts="+onesoa +noclass +nocomments +nostats +nocmd"
dig AXFR ${dig_opts} @b.root-servers.net. ${zone} > ./zones/root.zone
../../tools/gen_zonelist.sh
../../tools/gen_configs.sh
../../tools/gen_noerror.sh ./zones/root.zone
mv querydb querydb_noerror
../../tools/gen_nxdomain.sh querydb_noerror ${zone} 80
sed -i -E 's/$/\ DO/' querydb
../../tools/gen_notes.sh "ROOT" 1 NXDOMAIN 80 100
../../tools/pack_dataset.sh
