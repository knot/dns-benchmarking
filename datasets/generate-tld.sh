#!/bin/bash

for dataset in tld tld_ipv6 tld_dnssec tld_nsec3 tld_nsec
do
	pushd ${dataset}
	./generate.sh
	popd
done

ln -s tld.tgz tld_tcp.tgz
ln -s tld_ipv6.tgz tld_ipv6_tcp.tgz
