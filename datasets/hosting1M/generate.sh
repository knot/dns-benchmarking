#!/usr/bin/env bash
set -e

zone_count=1000000

mkdir -p zones
sed -e "s%@DOTZONE@%%g" -e "s%@ZONE@%@%g" ../../tools/zone.tpl > ./zones/template.zone
../../tools/gen_zonelist.py ${zone_count}
../../tools/gen_configs.sh zones/template.zone
../../tools/gen_noerror.sh stub.zone
../../tools/gen_notes.sh "Hosting (1M)" ${zone_count} NOERROR 100 0
../../tools/pack_dataset.sh
