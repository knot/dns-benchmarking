#!/usr/bin/env bash
set -e

zone="se"
ref="tld"

ln -s ../${ref}/zones
../../tools/gen_zonelist.sh
../../tools/gen_configs.sh
ln -s ../${ref}/querydb
../../tools/gen_notes.sh "TLD IPv6" 1 NOERROR 100 0
../../tools/pack_dataset.sh
