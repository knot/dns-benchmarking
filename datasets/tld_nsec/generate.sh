#!/usr/bin/env bash
set -e

zone="se"
ref="tld"

mkdir -p zones
BASEDIR=./zones ../../tools/sign_zone.sh ${zone} ../${ref}/zones/${zone}.zone ./zones/${zone}.zone nsec
../../tools/gen_zonelist.sh
../../tools/gen_configs.sh
ln -s ../${ref}_nsec3/querydb
../../tools/gen_notes.sh "TLD NSEC" 1 NXDOMAIN 100 100
../../tools/pack_dataset.sh
