#!/usr/bin/env bash
source ${HOOKDIR}/wait_start.hook

#
# BIND9 benchmark hook
#

export REPO="https://gitlab.isc.org/isc-projects/bind9.git"
export CONFIGURE_PARAMS="--disable-doh"
export BIND_NAME="bind"
export BIND_BIN_PATH="bin/named/named"
export SRC_DIR="${BASEDIR}/bind9.src"
export BIN="${SRC_DIR}/${BIND_BIN_PATH}"

function ns_info() {
	echo "BIND $(${BIN} -V|head -n1|awk '{print $2}')" | tee -a $MEASURE_LOG 
}

function ns_info_path() {
	echo "$(ns_info)' at '${BIN}" | tee -a $MEASURE_LOG
}

function ns_pid() {
	echo $PID | tee -a $MEASURE_LOG
}

function ns_install() {
	# Check out specific branch if set
	[ -d ${SRC_DIR} ] && rm -r ${SRC_DIR} &>>$LOG
	if ! git clone --depth 1 $REPO -b ${version} ${SRC_DIR} &>>$LOG; then
		echo "error: couldn't fetch '${version}' from '${REPO}'"
		return 1
	fi
	echo "checked out '${version}'" | tee -a $MEASURE_LOG

	# Build sources
	pushd ${SRC_DIR} > /dev/null
	autoreconf -if &>>$LOG
	CC=$CC CFLAGS=$CFLAGS ./configure ${CONFIGURE_PARAMS} &>>$LOG
	make -j${CPUS} &>>$LOG
	popd > /dev/null

	if [ ! -x ${BIN} ]; then
		echo "error: couldn't make an executable" | tee -a $MEASURE_LOG
		return 1a
	fi

	echo "built '$(ns_info_path)'" | tee -a $MEASURE_LOG
}

function ns_init() {
	version="main"
	[ -n "${1}" ] && version=${1}

	setup_bin_env ${BIND_BIN_PATH} ${BIND_NAME} ${bind9_bin}

	echo "using '$(ns_info_path)'" | tee -a $MEASURE_LOG
}

function ns_prep() {
	return 2 # Don't measure
}

function ns_start() {
	${BIN} -c ${1} -g -n ${CPUS} &>server.log &
	PID=$!

	wait_start "all zones loaded"
	return 0
}

function ns_stop() {
	wait_stop
}
