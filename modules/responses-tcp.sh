#!/usr/bin/env bash

function net_discover()
{
	UNIQ="guniq"
	if [ `uname` = "Linux" ]; then
		 UNIQ="uniq"
	fi

	ip4=""; ip6=""; mac=""
	addr=""; link=""
	if [ "`ssh $1 uname`" = "Linux" ]; then
		addr="$(ssh $1 ip -d -o addr|grep ": ${2}"|grep -o -E "(inet|inet6) ([^ ]+)"|grep -v Link|$UNIQ -w 5)"
		link="$(ssh $1 ip -d -o link|grep ": ${2}"|grep -o -E "(ether) ([^ ]+)"|$UNIQ -w 5)"
	else
		addr="$(ssh $1 ifconfig ${2}|grep -o -E "inet(6)? ([^ ]+)"|$UNIQ|grep -v -E "(fe80)|%")"
		link="$(ssh $1 ifconfig ${2}|grep -o -E "ether ([^ ]+)"|$UNIQ -w 5)"
	fi

	while read -r l; do
		l=( $(echo $l) )
		case ${l[0]} in
		inet)  ip4=( ${net_v4[@]} $(echo ${l[1]}|cut -d\/ -f1) ) ;;
		inet6) ip6=( ${net_v6[@]} $(echo ${l[1]}|cut -d\/ -f1) ) ;;
		esac
	done <<< "$addr"
	while read -r l; do
		l=( $(echo $l) )
		case ${l[0]} in
		ether) mac=( ${net_mac[@]} ${l[1]} ) ;;
		esac
	done <<< "$link"

	# Decide used address
	output=()

	if [[ ${IP_VERSION} -eq 4 ]]; then
		output=("${mac}" "${ip4}")
	else
		output=("${mac}" "${ip6}")
	fi

	echo ${output[@]}
}

function replay()
{
	duration="${replay_duration:-30}"

	# Send the queries
	if [ "$IP_VERSION" = "6" ]; then
		subnet="fd00::0/64"
	else
		subnet="10.0.0.0/8"
	fi
	cmd="sudo kxdpgun -t ${duration} -p ${PORT} -b 20 -Q ${1} -i ${2} ${3} ${KXDPGUN_OPTS} -T -l ${subnet}"
	out=( `ssh "${player[0]}" "${cmd}" 2>&1 | tee -a ${replay_log} | awk '/SYN|replies/ {print $3}'` )

	query_pkts=${out[0]}
	reply_pkts=${out[1]}
	
	# Calculate results
	answered=$(bc <<< "scale=2; ${reply_pkts}*100.0/${query_pkts}")
	query_rate=$(bc <<< "scale=0; ${query_pkts}/${duration}")
	reply_rate=$(bc <<< "scale=0; ${reply_pkts}/${duration}")
	query_avglen=0
	reply_avglen=`cat ${replay_log} | awk '/reply size/ {print $5}'`

	echo ${answered} ${query_rate} ${query_avglen} ${reply_rate} ${reply_avglen}

	# Wait for connection sweep
	sleep 42
}

function module_init()
{
	ETH_OVERHEAD=20

	if [ -n "$(echo ${2} | grep -i "ipv6")" ]; then
		IP_VERSION=6
		PKT_OVERHEAD=66
	else
		IP_VERSION=4
		PKT_OVERHEAD=46
	fi

	# Populate address list
	addr=( $(net_discover ${target} ${iface}) )
	ADDRLIST=( ${addr[1]} )
}

function generate_maximum()
{
	output_max="${BASEDIR}/results/max.data"

	network_type=`ethtool ${iface} 2> /dev/null | awk '/speed/ {print $2}' IGNORECASE=1`
	net_speed=`echo ${network_type} | sed 's/[^0-9]//g'`
	limit=`bc <<< "scale=0; ($net_speed * 1000 * 1000 / 8) / (${GLOBAL_REPLY_AVG_SIZE} + ${PKT_OVERHEAD} + ${ETH_OVERHEAD})"`
	# Replay the pcap files
	for pps in ${do_rates[@]}; do
		[ ${pps} -eq 0 ] && continue
		limit_answers=`bc <<< "scale=0; if(${limit} <= ${pps}) ${limit} else ${pps}"`
		percent=`bc <<< "scale=2; ${limit_answers} * 100.0 / ${pps}"`
		echo -e "${pps}\t${percent}\t${GLOBAL_REPLY_AVG_SIZE}" >> ${output_max}.tmp
	done

	# Save results
	echo -e "# ${network_type} limit,${GLOBAL_REPLY_AVG_SIZE}" > ${output_max}
	cat ${output_max}.tmp | sort -n >> ${output_max}
	rm ${output_max}.tmp
}

function module_exec()
{
	NS_NAME="${1}"
	output="${BASEDIR}/results/${NS_NAME}.data"
	replay_log="kxdpgun.log"
	GLOBAL_REPLY_COUNT=0
	GLOBAL_REPLY_SIZE=0

	# Drop filesystem and page caches
	sudo sync
	echo 3|sudo tee /proc/sys/vm/drop_caches &>/dev/null

	run_netsetup "${NS_NAME}"
	
	# Start nameserver
	ns_prep ${NAMECONF}
	ns_start ${NAMECONF}
	if [ $? -gt 0 ]; then
		echo "error: failed to start '${NS_NAME}'" | tee $MEASURE_LOG
		return 1
	fi
	prlimit -p `ns_pid` --nofile=1000000:1000000

	echo "" > ${replay_log}

	# Perform measurement of answered queries
	do_rates=(0)
	if [ "${replay_rates}" ]; then
		if [ "${replay_step}" ] && [ ${#replay_rates[@]} -ge 2 ]; then
			do_rates=( $(${SEQ} ${replay_rates[0]} ${replay_step} ${replay_rates[1]}) )
		else
			do_rates=${replay_rates[@]}
		fi
	fi

	QUERYDB="/tmp/querydb"
	player=( $(echo ${players[0]}|tr ':' ' ') )
	scp ${BASEDIR}/querydb ${player[0]}:${QUERYDB} &>>${LOG}

	# Update routing table with an artificial subnet
	net_dst=( $(net_discover ${target} ${iface}) )
	net_src=( $(net_discover ${player[0]} ${player[1]}) )
	if [ "$IP_VERSION" = "6" ]; then
		sudo ip route del fd00::0/64 2> /dev/null
		sudo ip route add fd00::0/64 via ${net_src[1]}
	else
		sudo ip route del 10.0.0.0/8 2> /dev/null
		sudo ip route add 10.0.0.0/8 via ${net_src[1]}
	fi

	# Replay the pcap files
	for pps in ${do_rates[@]}; do
		[ ${pps} -eq 0 ] && continue
		result=( $(replay ${pps} ${QUERYDB} ${net_dst[1]}) )

		reply_size_round=`printf '%.0f' ${result[4]}`
		GLOBAL_REPLY_COUNT=`bc <<< "${GLOBAL_REPLY_COUNT} + ${result[3]}"`
		GLOBAL_REPLY_SIZE=`bc <<< "${GLOBAL_REPLY_SIZE} + ${result[3]} * ${result[4]}"`

		echo "${NS_NAME} ${result[0]} % answered for ${result[1]} q/s, ${result[2]} B avg (${result[3]} a/s, ${reply_size_round} B avg)" | tee $MEASURE_LOG
		echo -e "${result[1]}\t${result[0]}\t${result[4]}" >> ${output}.tmp
	done

	GLOBAL_REPLY_AVG_SIZE=`bc <<< "scale=0; ${GLOBAL_REPLY_SIZE} / ${GLOBAL_REPLY_COUNT}"`

	[ "$1" = "${gen_max}" ] && generate_maximum

	# Stop nameserver
	ns_stop ${NAMECONF}

	# Save results
	echo -e "# $(ns_info),${GLOBAL_REPLY_AVG_SIZE}" > ${output}
	cat ${output}.tmp|sort -n >> ${output}
	rm ${output}.tmp
	cat ${replay_log} > "${BASEDIR}/results/kxdpgun-${NS_NAME}.log"
	sed '/TCP$/b;s/\(.*Protocol.*\)$/\1 TCP/' -i NOTES
}
