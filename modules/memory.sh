#
# Memory measurements
# 

function time_now() {
	date +%s
}

function mem_snapshot()
{
	sync
	free -m|awk '{print $3}'|tail -n 2
        return $?
}

function module_init()
{
	ADDRLIST=( 127.0.0.1 )
}

function module_exec()
{
	# Drop filesystem and page caches
	sudo sync
	echo 3|sudo tee /proc/sys/vm/drop_caches &>/dev/null

        # Start nameserver
	t_base=$(time_now)
        ns_prep ${NAMECONF}
	ret=$?
	t_prep=$(time_now)
	# Don't measure prep phase if failed
	if [ $ret -ne 0 ]; then
		t_prep=${t_base}
	fi

	# Drop filesystem and page caches
	sudo sync
	echo 3|sudo tee /proc/sys/vm/drop_caches &>/dev/null

	# Take memory snapshot
	mem_free=( $(mem_snapshot) )
        ns_start ${NAMECONF}
        if [ $? -gt 0 ]; then
                echo "error: failed to start '${1}'"
                return 1
        fi
	t_start=$(time_now)

	# Calculate times
	dt_prep=$(( ${t_prep} - ${t_base} ))
	dt_start=$(( ${t_start} - ${t_prep} ))

	# Measure memory and stop server
	mem_used=( $(mem_snapshot) )
	ns_stop ${NAMECONF}

	# Calculate delta of free memory
	# This is not very precise, but with the c-o-w and different
	# threading/process models of different servers, it's one of the
	# comparable values
	mem_dt=()
	i=0; while [ ${i} -lt ${#mem_used[@]} ]; do
		mem_dt[${i}]=$(( ${mem_used[$i]} - ${mem_free[$i]} ))
		[ ${mem_dt[$i]} -lt 0 ] && mem_dt[$i]=0
		(( i += 1 ))
	done

	# Write out results
	output="${BASEDIR}/results/usage"
	[ ! -f ${output} ] && echo "# Compile [s] | Startup [s] | Rss [MB] | Swap [MB]" > ${output}
	echo -e "\"$(ns_info)\"\t${dt_prep}\t${dt_start}\t${mem_dt[0]}\t${mem_dt[1]}" >> ${output}
}
