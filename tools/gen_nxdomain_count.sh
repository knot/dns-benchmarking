#!/usr/bin/env bash

if [ $# -ne 3 ]; then
        echo "Usage: $0 <noerror_querydb/zone_file> <zone_name> <count>"
        exit 1
fi

NOERRORS=${1}
ORIGIN=${2}
COUNT=${3}

DOT_ORIGIN=$ORIGIN
if [ "$ORIGIN" != "." ]; then
	DOT_ORIGIN=.$ORIGIN
fi

URANDOM=/dev/urandom
TYPES=(A AAAA NS DS)
NTYPES=${#TYPES[@]}

function rnd_word() {
	WORD_LEN=$((RANDOM % 30 + 1))
	< /dev/urandom tr -dc a-z0-9 | head -c $WORD_LEN
}

function rnd_type() {
	NTYPE=$((RANDOM % NTYPES))
	echo "${TYPES[$NTYPE]}"
}

NRESULT=0
while [ $NRESULT -lt $COUNT ]; do
	QNAME=$(rnd_word)$DOT_ORIGIN
	if grep -q '^[.a-zA-Z0-9]*'$QNAME'[[:space:]]' $NOERRORS; then
		continue
	fi
	echo $QNAME $(rnd_type)
	NRESULT=$((NRESULT + 1))
done
