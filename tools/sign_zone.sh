#!/bin/bash

export SIGNKEY=""
export KSK=""
export STYPE="-3"
export ITERS="-H 0"
export ENDTIME="20500101000000"
export SALT="-"
export ALG="ECDSAP256SHA256"

_keygen() {
	keygenlog=${BASEDIR}/keygen.log
	echo -n > $keygenlog
	key=$(dnssec-keygen $STYPE        -a $ALG -n ZONE -K $BASEDIR $ZONE 2>>$keygenlog)
	export SIGNKEY=${key}
	key=$(dnssec-keygen $STYPE -f KSK -a $ALG -n ZONE -K $BASEDIR $ZONE 2>>$keygenlog)
	export KSK=${key}
	#echo "\$include $SIGNKEY.key ; ZSK" >> $ZFILE_OUT
	#echo "\$include $KSK.key ; KSK" >> $ZFILE_OUT
}

_sign_zone() {
	flags=""
	if [ "$STYPE" == "-3" ]; then
		flags="$STYPE $SALT $ITERS"
	fi
	dnssec-signzone $flags -O full -d $BASEDIR -K $BASEDIR -k ${KSK} -e $ENDTIME \
	                -S -o $ZONE -f $2 $1 $SIGNKEY.key &>>$LOG
}

if [ $# -lt 3 ]; then
	echo "Usage: $0 <zone> <zonefile_in> <zonefile_out> [nsec|nsec3] [expiration]"
	exit 1
fi

if [ -z $BASEDIR ]; then
	export BASEDIR=$(pwd)
fi
export LOG=.log
export ZONE=$1
export ZFILE_IN=$2
export ZFILE_OUT=$3
if [ $# -gt 3 ] && [ "$4" == "nsec" ]; then
	STYPE=""
fi
if [ $# -gt 4 ] && [ -n "$5" ]; then
	ENDTIME=$5
fi
if [ -f .skey ] && [ -f .ksk ]; then
	export SIGNKEY=$(cat .skey)
	export KSK=$(cat .ksk)
else
	_keygen
	echo $SIGNKEY > .skey
	echo $KSK > .ksk
	mv -u .skey $BASEDIR &>>$LOG
	mv -u .ksk $BASEDIR &>>$LOG
fi
_sign_zone $ZFILE_IN $ZFILE_OUT
cat $LOG
rm $LOG
