#!/usr/bin/env bash

dataset=$(basename $(pwd))
filename="${dataset}.tgz"

echo -n "Packing: ${dataset} ..."
if [ ! -f NOTES ]; then
	echo " missing NOTES"
	exit 1
fi

rm -f ../${filename}
tar hczf ../${filename} NOTES `ls *.conf *.pcap *.mmdb 2>/dev/null` zonelist zones querydb \
`[ -d keys ] && echo -n "keys"` || rm ../${filename}
echo -e "\t($(du -sh ../${filename}))"
