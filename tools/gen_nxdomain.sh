#!/usr/bin/env bash

if [ $# -ne 3 ]; then
	echo "Usage: $0 <noerror_querydb> <zone_name> <nxdomain_percent>"
	exit 1
fi

noerrors=${1}
zone=${2}
[ "${zone: -1}" != "." ] && zone="${zone}."
nxd_pct=${3}

base=`dirname ${BASH_SOURCE[0]}`

if [ ${nxd_pct} -eq 100 ]; then
	# Create NXDOMAIN queries from NOERROR ones, filter out oversized labels
	sed -e "s/\(\.${zone}\ \)/_n1x9d\1/" -e "s/\(^${zone}\ \)/n1x9d.\1/" < ${noerrors} | egrep -v '[^.]{64,}' > querydb
else
	# Mix the NOERROR and NXDOMAIN queries to get the desired proportion
	noerror_count=$(wc -l < ${noerrors})
	nxdomain_count=$(( ${noerror_count} * ${nxd_pct}/(100 - ${nxd_pct}) ))
	tmpf=$(mktemp)
	$base/gen_nxdomain_count.sh ${noerrors} ${zone} ${nxdomain_count} > ${tmpf}
	cat ${noerrors} ${tmpf} | sort -R > querydb
	rm ${tmpf}
fi
