#!/usr/bin/env bash

if [ $# -ne 5 ]; then
	echo "Usage: $0 <dataset_name> <zone_count> <rcode> <rcode_pct> <do_pct>"
	exit 1
fi

ZONE_DIR="zones"
OUT_FILE="NOTES"

dataset=$1
zones=$2
rcode=$3
rcode_pct=$4
do_pct=$5

dataset_len=${#dataset}
zone="$(ls ${ZONE_DIR}/*.zone | head -n 1)"

records=$(cat $zone | grep -v "\s*;" | wc -l)
delegs=$(cat $zone | sed -E 's/\sIN\s/ /' | awk '{print $1 " " $3}' | grep -e "\sNS$" | sort -u | wc -l)

proto="IPv4"
[ -n "$(basename "$PWD" | grep -i "ipv6")" ] && proto="IPv6"

dnssec="no"
dnskey=$(cat $zone | grep "\sDNSKEY\s" | grep -m 1 -v "\sRRSIG\s")
nsec3param=$(cat $zone | grep -m 1 "\sNSEC3PARAM\s")
if [ -n "${dnskey}" ]; then
  dnssec=$(echo ${dnskey} | sed 's/^.*DNSKEY/DNSKEY/' | awk '{print "yes, algorithm " $4}')
  if [ -n "${nsec3param}" ]; then
    nsec3_rdata=$(echo $nsec3param | sed 's/^.*NSEC3PARAM/NSEC3PARAM/')
    iters=$(echo ${nsec3_rdata} | awk '{print $4}')
    salt=$(echo ${nsec3_rdata} | awk '{print $5}')
    dnssec="${dnssec}, NSEC3 (${iters} iterations, $(( ${#salt}/2 ))-octet salt)"
  else
    dnssec="${dnssec}, NSEC"
  fi
fi

printf "%s\n" "${dataset}" > ${OUT_FILE}
printf "=%.0s" $(eval "echo {1..${dataset_len}}") >> ${OUT_FILE}
printf "\n\n" >> ${OUT_FILE}
printf "* Zones: %s\n" "${zones}" >> ${OUT_FILE}
printf "* DNSSEC: %s\n" "${dnssec}" >> ${OUT_FILE}
printf "* Records: %s total, %s delegations\n" "${records}" "${delegs}" >> ${OUT_FILE}
printf "* Queries: random QNAME, %s%% DO\n" "${do_pct}" >> ${OUT_FILE}
printf "* Replies: %s%% %s\n" "${rcode_pct}" "${rcode}" >> ${OUT_FILE}
printf "* Protocol: %s\n" "${proto}" >> ${OUT_FILE}
