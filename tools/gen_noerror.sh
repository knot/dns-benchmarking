#!/usr/bin/env bash

if [ $# -ne 1 ]; then
	echo "Usage: $0 <zonefile>"
	exit 1
fi

zonefile=${1}

# Check if the zone file contains IN class column
line=$(grep -m 1 "SOA" ${zonefile} | awk '{print $3}')
awk_expr="{print \$1,\$3}"
[ "${line}" == "IN" ] && awk_expr="{print \$1,\$4}"

# Extract suitable records and create NOERROR queries
tmpf=$(mktemp)
awk "${awk_expr}" ${zonefile} | grep -E "(NS|DS|A|AAAA|PTR|MX|SOA|DNSKEY)$" | sort -u -R > ${tmpf}
mv -f ${tmpf} querydb
