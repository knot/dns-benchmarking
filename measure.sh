#!/usr/bin/env bash

# Check and load up configuration
CONFIG="$1"
if [ ! -f $CONFIG ]; then
	echo "Usage: $0 <configuration> [flags]"
	exit 2
fi

source $CONFIG
iface=$2

# Constants
DATADIR=$(dirname $0)
export BASEDIR="${DATADIR}/data"
ZONEDIR="${BASEDIR}/zones"
ZONELIST="${BASEDIR}/zonelist"
HOOKDIR="${DATADIR}/hooks"
MODULEDIR="${DATADIR}/modules"
LOG="${BASEDIR}/results/dnsbench.log"
MEASURE_LOG="${BASEDIR}/results/benchmark.log"
NAMECONF="conf.run"
if [ "${host_port}" == "" ]; then
	PORT=$(( $RANDOM % 100 + 50000 ))
else
	PORT=${host_port}
fi
ADDRLIST=()
OLD_CONF_CNT=1

# Override compiler flags
PATH="/usr/lib/ccache:${PATH}"
CFLAGS="-O2 -g -DNDEBUG"
PROF_CFLAGS="-Og -ggdb -g3 -fno-omit-frame-pointer -DNDEBUG"
CXXFLAGS="${CFLAGS}"

# Unpack dataset and prepare
[ -d $BASEDIR ] && rm -rf $BASEDIR
mkdir -p $BASEDIR/wd; cd $BASEDIR
if [ ! -f ${DATADIR}/${dset}.tgz ]; then
	echo "error: '${dset}' is not valid dataset"
	exit 2
else
	tar xzf ${DATADIR}/${dset}.tgz
fi
ln -s ../zones wd/


mkdir ${BASEDIR}/results
touch $MEASURE_LOG
# Print host port needed for online observation
echo "selected host port: ${PORT}" | tee -a $MEASURE_LOG

# Discover seq implementation
SEQ="gseq"
which $SEQ &>/dev/null || SEQ="seq"

# Prepare name server configuration
function bench_init() {
	# Rewrite rules
	if [ -z "${CPUS}" ]; then
		if [ `uname` = 'Linux' ]; then
			CPUS=$(lscpu|grep "^CPU(s):"|awk '{print $2}')
		else
			CPUS=$(sysctl hw.ncpu | cut -d" " -f2)
		fi
	fi
	USER="$(whoami)"

	# Parse hook parameter
	hook_param=( $(echo ${1}|tr "@" " ") )

	# Generate configuration from template
	[ -f ${hook_param[0]}.conf ] && source ${hook_param[0]}.conf > ${NAMECONF}

	# Load hooks for target nameserver
	if [ ! -f ${HOOKDIR}/${hook_param[0]}.hook ]; then
		echo "error: can't find hook for '${hook_param[0]}'" | tee -a $MEASURE_LOG
		return 1
	fi

	# Get required version
	version=
	variable=${hook_param[0]}_version
	[ ${!variable} ] && version=${!variable}
	[ ${hook_param[1]} ] && version=${hook_param[1]}

	# Initialize hook
	source ${HOOKDIR}/${hook_param[0]}.hook

	ns_init ${version}
	return $?
}

# Clean up after benchmark
function bench_deinit() {
	if [ -f server.log ]; then
		cat server.log >> $LOG; rm server.log
		mv ${NAMECONF} old.conf.${OLD_CONF_CNT}
                ((OLD_CONF_CNT++))
	fi
}

# Verify connection
cd $BASEDIR
for h in ${hostnames[@]}; do
	ssh -o BatchMode=yes -o ConnectTimeout=5 $h uname &>/dev/null
	if [ $? -ne 0 ]; then
		echo "error: can't establish an ssh connection to '$h'" | tee -a $MEASURE_LOG
		exit 2
	fi
done

# Begin measurements
for bench_module in ${modules[@]}; do
	echo "benchmarking module '${bench_module}'" | tee -a $MEASURE_LOG
	for s in ${configs[@]}; do
		nsname=${s%%.conf}

		# Declare dummy prototype
		function module_exec() { return 1; }

		# Load an actual module
		source ${MODULEDIR}/${bench_module}.sh
		module_init ${nsname} ${dset}

		# Run benchmark
		bench_init ${nsname}
		if [ $? -eq 0 ]; then
			module_exec ${nsname} ${dset}
		fi
		bench_deinit
	done
done

# Save results
cd ${BASEDIR}/results
cp ${BASEDIR}/NOTES .
if [ "${host_os}" == "" ]; then
	host_os=$(uname -s -r)
fi
echo "${host_os} \"${iface_info}\"" > INFO
tar cvzf ${DATADIR}/results.tgz * &>/dev/null
cd -
rm -r ${BASEDIR}/results &>/dev/null

