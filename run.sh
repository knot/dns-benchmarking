#!/bin/bash

for dataset in tld tld_ipv6 tld_dnssec tld_nsec tld_nsec3 hosting1M root
do
	./dnsbench.sh $1 datasets/${dataset}.tgz
done | tee results/run-$(date +%s).log
